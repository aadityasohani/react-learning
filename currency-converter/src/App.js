
// import { useState } from 'react';
import './App.css';
import CurrencyConverter from './components/CurrencyConverter';
import TableViewer from './components/TableViewer';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Navigation from './components/Navigation';
function App() {
  // const [disp,setDisp] = useState(false);
  // const [btnData,setBtnData]=useState("show json data");
  return (
    //   <button onClick={()=>{setDisp(!disp); setBtnData(btnData==="show json data"?"show currency-converter":"show json data")}}>{btnData}</button>
    //   {disp?
    //     <TableViewer/>
    //   :<CurrencyConverter/>}
  

    <BrowserRouter>
    <div className="App">
      <Navigation/>
      <Routes>
        <Route path="/" element={<CurrencyConverter/>} />
        <Route path="/apiCall" element ={<TableViewer/>}/>
      </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
