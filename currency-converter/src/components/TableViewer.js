import { useEffect, useState } from "react";
import './TableViewer.css';

const TableViewer = ()=>{
    const [data,setData] = useState([]);
    const [start,setStart] = useState(0);
    const [end,setEnd] = useState(10);
    const url = "https://jsonplaceholder.typicode.com/posts";
    const getData = ()=>{
        fetch(url).then((res)=>{
            return res.json()
        }).then((d)=>{
            setData(d);
        }).catch(e=>{
            console.log(e);
        })
    }
    useEffect(()=>{
        getData();

    },[]);
    return(
        <div className="table">
            <table style={{"border":"1px solid black"}}>
                <tbody>
                <tr>
                    <th>userId</th>
                    <th>id</th>
                    <th>title</th>
                    <th>body</th>
                </tr>
                {data.slice(start,end).map((d)=>{
                    return(
                        <tr key={d.id}>
                            <td>{d.userId}</td>
                            <td>{d.id}</td>
                            <td>{d.title.slice(0,30)}...</td>
                            <td>{d.body.slice(0,30)}....</td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
            <button onClick={(e)=>{setEnd(start===0?data.length:start);setStart(start===0?data.length-10:start-10);}}> prev </button>
            <button onClick={(e)=>{setStart(end===data.length?0:end);setEnd(end===data.length?10:end+10);}}>next</button>
        </div>
    )
}

export default TableViewer;