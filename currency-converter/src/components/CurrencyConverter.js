import { useEffect, useState } from "react";
import './CurrencyConverter.css';

const CurrencyConverter = () =>{
    var [inpCurr,setInpCurr] = useState(0);
    var [inpCurrType,setInpCurrType]=useState("inr");

    var [outCurr,setOutCurr]=useState(0);
    var [outCurrType,setOutCurrType]= useState("inr");

    var options = ["inr","usd","euro","din","cd"];
    var flags = {"inr":"🇮🇳","usd":"🇺🇸","euro":"🇩🇪","din":"🇪🇬","cd":"🇨🇦"};



    

    useEffect(()=>{
        var currValues = {"inr":1,"usd":75.15,"euro":84.08,"din":248.18,"cd":59.17};
        const findOutput = () =>{
            var ans = Math.round(((inpCurr *currValues[inpCurrType])/currValues[outCurrType])*100)/100;
            setOutCurr(ans);
        }
        findOutput();
    },[inpCurr,inpCurrType,outCurrType]);
    
    
    return(
        <div className="currency">
            <h1>Currency Converter</h1>
            <div className="inp">
                <input value = {inpCurr} onChange={(e)=>{
                    setInpCurr(e.target.value);
                }
                    }></input>
                <select value = {inpCurrType} onChange={(e)=>{
                    setInpCurrType(e.target.value);
                    
                }}>
                    {options.map((opt)=>{
                        return(
                            <option value = {opt} key={opt}>
                                {opt.toUpperCase()}&nbsp;{flags[opt]}
                            </option>
                        )
                    })}
                </select>
            </div>
            <br/>
            <input value = {outCurr} readOnly></input>
            <select value = {outCurrType} onChange={(e)=>{
                setOutCurrType(e.target.value);
            }}>
                {options.map((opt)=>{
                    return(
                        <option value = {opt} key={opt}>
                            {opt.toUpperCase()}&nbsp;{flags[opt]}
                        </option>
                    )
                })}
            </select>
        </div>
    )
}


export default CurrencyConverter;