import {Link, Redirect} from 'react-router-dom';
import './Navigation.css'

const Navigation = ()=>{
    return(
        <nav>
            <ul>
                <li>
                    <Link to="/" >
                        currency
                    </Link>
                </li>
                <li>
                    <Link to="/apiCall" >
                        use Api
                    </Link>
                </li>
            </ul>
        </nav>
    )
}

export default Navigation;